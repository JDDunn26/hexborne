﻿using System.Collections.Generic;
using UnityEngine;

public class GameEventBase : ScriptableObject
{
    protected readonly List<IGameEventListener> _eventListeners = new List<IGameEventListener>();

#if UNITY_EDITOR
    [TextArea(5, 8)]
    public string devDescription = string.Empty;
#endif
}