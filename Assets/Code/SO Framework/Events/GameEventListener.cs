using System;
using System.Linq;
using System.Reflection;
using UnityEngine;

public interface IGameEventListener
{
    void OnEventRaised();
    void OnEventRaised<T0>(T0 arg0);
    void OnEventRaised<T0, T1>(T0 arg0, T1 arg1);
    void OnEventRaised<T0, T1, T2>(T0 arg0, T1 arg1, T2 arg2);
    void OnEventRaised<T0, T1, T2, T3>(T0 arg0, T1 arg1, T2 arg2, T3 arg3);
}

public class GameEventListener : MonoBehaviour, IGameEventListener
{
    public GameEvent _event;
    public CustomEvent _response;

    private void OnEnable()
    {
        _event?.RegisterListener(this);
    }

    private void OnDisable()
    {
        _event?.UnregisterListener(this);
    }

    public void ChangeEvent(GameEvent gEvent)
    {
        if (_event != null)
        {
            _event.UnregisterListener(this);
        }
        _event = gEvent;

        if (_event != null)
        {
            _event.RegisterListener(this);
        }
    }

    public void OnEventRaised()
    {
        _response.Invoke();
    }

    public void OnEventRaised<T0>(T0 arg0)
    {
        object[] args = { arg0 };
        HandleMethodInvoke(args, new[] { typeof(T0) });
    }

    public void OnEventRaised<T0,T1>(T0 arg0, T1 arg1)
    {
        object[] args = { arg0, arg1 };
        HandleMethodInvoke(args, new[] { typeof(T0), typeof(T1) });
    }

    public void OnEventRaised<T0, T1, T2>(T0 arg0, T1 arg1, T2 arg2)
    {
        object[] args = { arg0, arg1, arg2 };
        HandleMethodInvoke(args, new[] { typeof(T0), typeof(T1), typeof(T2) });
    }

    public void OnEventRaised<T0, T1, T2, T3>(T0 arg0, T1 arg1, T2 arg2, T3 arg3)
    {
        object[] args = { arg0, arg1, arg2, arg3 };
        HandleMethodInvoke(args, new[] { typeof(T0), typeof(T1), typeof(T2), typeof(T3) });
    }

    private void HandleMethodInvoke(object[] args, Type[] argTypes)
    {
        int eventCount = _response.GetPersistentEventCount();

        for (int i = 0; i < eventCount; i++)
        {
            MethodInfo methodInfo = GetValidMethodInfo(_response.GetPersistentTarget(i), _response.GetPersistentMethodName(i), argTypes, out var argCount);
            
            if (methodInfo != null)
            {
                Debug.Log(argCount + " : " + methodInfo.Name);
                methodInfo.Invoke(_response.GetPersistentTarget(i) as Component, args.Take(argCount).ToArray());
            }           
        }
    }

    private static MethodInfo GetValidMethodInfo(object obj, string functionName, Type[] argumentTypes, out int count)
	{
		Type type = obj.GetType();
		MethodInfo result;
        count = 0;
        
        while (type != typeof(object) && type != null)
		{
            MethodInfo nearestMatch = type.GetMethod(functionName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[0], null);
           
            for (int i = 0; i < argumentTypes.Length; i++)
            {
                MethodInfo method = type.GetMethod(functionName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, argumentTypes.Take(i + 1).ToArray(), null);
                if (method != null)
                {
                    nearestMatch = method;
                }
                count++;
            }
            
            type = type.BaseType;
            
            if (nearestMatch != null)
            {
                result = nearestMatch;
                return result;
            }
		}
        
		result = null;
		
		return result;
	}
}