﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu]
public class GameEvent : GameEventBase
{
    #region NETWORK LOGIC

    public static readonly Dictionary<short, GameEvent> NetworkGameEvents = new Dictionary<short, GameEvent>();
    public static readonly Queue<short> NetworkOutgoing = new Queue<short>();

    public enum MessageType
    {
        All, Others
    }

    public bool isNetworked;
    public short networkId = -1;
    public MessageType networkMessageType;

    public void OnEnable()
    {
        if (debug)
            Debug.Log($"{name}:{GetInstanceID()} has been loaded!");

        if (isNetworked)
        {
            networkId = (short)name.GetHashCode();
            if (debug)
                Debug.Log($"Adding {name}:{networkId} to network event list");
            if (NetworkGameEvents.ContainsKey(networkId))
                NetworkGameEvents[networkId] = this;
            else
                NetworkGameEvents.Add(networkId, this);
        }
    }

    #endregion
    
    public bool debug;

    public void Invoke(bool networkInvoked = false)
    {
        if (debug)
            Debug.Log($"Invoking {name}:{GetInstanceID()} event with 0 args {name} and {_eventListeners.Count} listeners");
        for (int i = _eventListeners.Count - 1; i >= 0; i--)
            _eventListeners[i].OnEventRaised();

        if (!networkInvoked && isNetworked)
            NetworkOutgoing.Enqueue(networkId);
    }

    public void Invoke<T0>(T0 arg0, bool networkInvoked = false)
    {
        if (debug)
            Debug.Log($"Invoking {name}:{GetInstanceID()} event with 1 args {name} and {_eventListeners.Count} listeners");
        
        for (int i = _eventListeners.Count - 1; i >= 0; i--)
            _eventListeners[i].OnEventRaised(arg0);

        if (!networkInvoked && isNetworked)
        {
            NetworkOutgoing.Enqueue(networkId);
        }
    }

    public void Invoke<T0, T1>(T0 arg0, T1 arg1, bool networkInvoked = false)
    {
        if (debug)
            Debug.Log($"Invoking {name}:{GetInstanceID()} event with 2 args {name} and {_eventListeners.Count} listeners");
        
        for (int i = _eventListeners.Count - 1; i >= 0; i--)
            _eventListeners[i].OnEventRaised(arg0, arg1);

        if (!networkInvoked && isNetworked)
            NetworkOutgoing.Enqueue(networkId);
    }

    public void Invoke<T0, T1, T2>(T0 arg0, T1 arg1, T2 arg2, bool networkInvoked = false)
    {
        if (debug)
            Debug.Log($"Invoking {name}:{GetInstanceID()} event with 3 args {name} and {_eventListeners.Count} listeners");
        
        for (int i = _eventListeners.Count - 1; i >= 0; i--)
            _eventListeners[i].OnEventRaised(arg0, arg1, arg2);

        if (!networkInvoked && isNetworked)
            NetworkOutgoing.Enqueue(networkId);
    }

    public void Invoke<T0, T1, T2, T3>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, bool networkInvoked = false)
    {
        if (debug)
            Debug.Log($"Invoking {name}:{GetInstanceID()} event with 4 args {name} and {_eventListeners.Count} listeners");
        
        for (int i = _eventListeners.Count - 1; i >= 0; i--)
            _eventListeners[i].OnEventRaised(arg0, arg1, arg2, arg3);

        if (!networkInvoked && isNetworked)
            NetworkOutgoing.Enqueue(networkId);
    }

    public void RegisterListener(IGameEventListener listener)
    {
        if (!_eventListeners.Contains(listener))
        {
            _eventListeners.Add(listener);
            if (debug)
                Debug.Log($"Added {listener.GetType()} as listener to {name}:{GetInstanceID()} game event ({_eventListeners.Count})");
        }
    }

    public void UnregisterAllListeners()
    {
        _eventListeners.Clear();
    }

    public void UnregisterListener(IGameEventListener listener)
    {
        if (_eventListeners.Contains(listener))
            _eventListeners.Remove(listener);
    }
}