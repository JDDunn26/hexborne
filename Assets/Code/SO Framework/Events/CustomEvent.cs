﻿using UnityEngine.Events;

[System.Serializable]
public class CustomEvent : UnityEvent
{
}

[System.Serializable]
public class CustomEvent<T0> : UnityEvent<T0>
{

}

[System.Serializable]
public class CustomEvent<T0, T1> : UnityEvent<T0, T1>
{

}
