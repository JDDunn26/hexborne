﻿using UnityEngine;
using UnityEngine.Networking;

[CreateAssetMenu(menuName = "SO Variables/Float")]
public class FloatVariable : VariableBase
{
    [SerializeField, HideInInspector]
    private float _defaultValue;

    [SerializeField, HideInInspector]
    private float _currentValue;

    public float Value
    {
        get => _currentValue;
        set
        {
            _currentValue = value;
            _objectValue = value;
        }
    }

    protected void OnEnable()
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
        _currentValue = _defaultValue;
        _objectValue = _defaultValue;
    }

    /// <summary>
    /// Set new value for float using variable of float type
    /// </summary>
    /// <param name="newValue"></param>
    public void SetValue(float newValue)
    {
        _currentValue = newValue;
        _objectValue = newValue;
        OnValueChanged.SafeInvoke();
    }

    /// <summary>
    /// Copy value from another FloatVariable
    /// </summary>
    /// <param name="newValue"></param>
    public void SetValue(FloatVariable newValue)
    {
        _currentValue = newValue.Value;
        _objectValue = newValue.Value;
        OnValueChanged.SafeInvoke();
    }

    /// <summary>
    /// Add float to current value
    /// </summary>
    /// <param name="amount"></param>
    public void ApplyChange(float amount)
    {
        _currentValue += amount;
        _objectValue = _currentValue;
        OnValueChanged.SafeInvoke();
    }

    /// <summary>
    /// Add value of another FloatVariable to current value
    /// </summary>
    /// <param name="amount"></param>
    public void ApplyChange(FloatVariable amount)
    {
        _currentValue += amount.Value;
        _objectValue = _currentValue;
        OnValueChanged.SafeInvoke();
    }

    public override void ResetData()
    {
        _currentValue = _defaultValue;
        _objectValue = _defaultValue;
    }

    protected override void WriteValueData(NetworkWriter writer)
    {
        writer.Write(Value);
    }

    protected override void ReadValueData(NetworkReader reader)
    {
        SetValue(reader.ReadSingle());
    }
}
