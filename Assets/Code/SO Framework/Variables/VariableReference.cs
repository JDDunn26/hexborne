﻿using UnityEngine;
using System;

[Serializable]
public abstract class VariableReference<T, U> where T : VariableBase
{
    public bool _useConstant = true;
    public U _constantValue;
    public T _variable;

    public T Value;

    public Action OnValueChanged;

    public VariableReference()
    {
        Setup();
    }

    public VariableReference(U value)
    {
        _useConstant = true;
        _constantValue = value;
    }

    ~VariableReference()
    {
        if (_variable != null)
            _variable.OnValueChanged -= HandleValueChange;
    }

    public void Setup()
    {
        if (!_useConstant && _variable != null)
            _variable.OnValueChanged += HandleValueChange;
    }

    public void Unload()
    {
        if (_useConstant)
            _variable.Unload();
    }

    protected virtual void HandleValueChange()
    {
        if (!Application.isPlaying || _useConstant)
            return;

        OnValueChanged.SafeInvoke();
    }
}

[Serializable]
public class FloatReference : VariableReference<FloatVariable, float>
{
    public void SetValue(float newValue)
    {
        if (_useConstant)
        {
            _constantValue = newValue;
        }
        else
        {
            Value = newValue;
        }
    }

    public void SetValue(FloatVariable newValue)
    {
        if (_useConstant)
        {
            _constantValue = newValue.Value;
        }
        else
        {
            Value = newValue.Value;
        }
    }

    public void ApplyChange(float amount)
    {
        if (_useConstant)
        {
            _constantValue += amount;
        }
        else
        {
            Value += amount;
        }
    }

    public void ApplyChange(FloatVariable amount)
    {
        if (_useConstant)
        {
            _constantValue += amount.Value;
        }
        else
        {
            Value += amount.Value;
        }
    }
    
    public new float Value
    {
        get => _useConstant ? _constantValue : _variable.Value;
        set
        {
            if (_useConstant)
                _constantValue = value;
            else
            {
                _variable.SetValue(value);    
                HandleValueChange();
            }
        }
    }
}

[Serializable]
public class StringReference : VariableReference<StringVariable, string>
{
    public new string Value
    {
        get => _useConstant ? _constantValue : _variable.Value;
        set
        {
            if (_useConstant)
                _constantValue = value;
            else
            {
                _variable.SetValue(value);
                HandleValueChange();
            }
        }
    }
}

[Serializable]
public class IntReference : VariableReference<IntVariable, int>
{
    public new int Value
    {
        get => _useConstant ? _constantValue : _variable.Value;
        set
        {
            if (_useConstant)
                _constantValue = value;
            else
            {
                _variable.SetValue(value);
                HandleValueChange();
            }
        }
    }
}

[Serializable]
public class ColorReference : VariableReference<ColorVariable, Color32>
{
    public new Color32 Value
    {
        get => _useConstant ? _constantValue : _variable.Value;
        set
        {
            if (_useConstant)
                _constantValue = value;
            else
            {
                _variable.SetValue(value);
                HandleValueChange();
            }
        }
    }
}

[Serializable]
public class BoolReference : VariableReference<BoolVariable, bool>
{
    public new bool Value
    {
        get => _useConstant ? _constantValue : _variable.Value;
        set
        {
            if (_useConstant)
                _constantValue = value;
            else
            {
                _variable.SetValue(value);
                HandleValueChange();
            }
        }
    }
}

