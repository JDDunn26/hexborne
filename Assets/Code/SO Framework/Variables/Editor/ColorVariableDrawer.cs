﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ColorVariable))]
public class ColorVariableDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        Rect varField = new Rect(position.x, position.y, position.width - 24, position.height);
        Rect colorField = new Rect(position.x + varField.width + 5, position.y, position.width - varField.width - 5,
            position.height);

        EditorGUI.PropertyField(varField, property);

        if (property.objectReferenceValue != null)
        {
            SerializedObject SO = new SerializedObject(property.objectReferenceValue);

            var colorProp = SO.FindProperty(Application.isPlaying ? "_currentValue" : "_defaultValue");

            colorProp.colorValue =
                EditorGUI.ColorField(colorField, GUIContent.none, colorProp.colorValue, false, true, false);

            SO.ApplyModifiedProperties();
        }
    }
}