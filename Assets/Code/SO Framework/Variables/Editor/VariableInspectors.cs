﻿using UnityEngine;
using UnityEditor;

public abstract class VariableInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Separator();

        serializedObject.Update();

        EditorGUILayout.PropertyField(serializedObject.FindProperty(Application.isPlaying ? "_currentValue" : "_defaultValue"), new GUIContent("Value"));

        serializedObject.ApplyModifiedProperties();
    }
}

[CustomEditor(typeof(FloatVariable))]
public class FloatVariableInspector : VariableInspector { }

[CustomEditor(typeof(StringVariable))]
public class StringVariableInspector : VariableInspector { }

[CustomEditor(typeof(IntVariable))]
public class IntVariableInspector : VariableInspector { }

[CustomEditor(typeof(ColorVariable))]
public class ColorVariableInspector : VariableInspector { }

[CustomEditor(typeof(BoolVariable))]
public class BoolVariableInspector : VariableInspector { }