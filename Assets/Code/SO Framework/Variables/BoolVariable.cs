﻿using UnityEngine;
using UnityEngine.Networking;

[CreateAssetMenu(menuName = "SO Variables/Boolean")]
public class BoolVariable : VariableBase
{
    [SerializeField, HideInInspector]
    private bool _defaultValue;

    [SerializeField, HideInInspector]
    private bool _currentValue;

    public bool Value
    {
        get { return _currentValue; }
        set
        {
            _currentValue = value;
            _objectValue = value;
        }
    }

    protected void OnEnable()
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
        _currentValue = _defaultValue;
        _objectValue = _defaultValue;
    }

    public void SetValue(bool newvalue)
    {
        Value = newvalue;
        OnValueChanged.SafeInvoke();
    }

    public void SetValue(BoolVariable newValue)
    {
        Value = newValue.Value;
        OnValueChanged.SafeInvoke();
    }

    public override void ResetData()
    {
        _currentValue = _defaultValue;
        _objectValue = _defaultValue;
    }

    protected override void WriteValueData(NetworkWriter writer)
    {
        writer.Write(Value);
    }

    protected override void ReadValueData(NetworkReader reader)
    {
        SetValue(reader.ReadBoolean());
    }
}