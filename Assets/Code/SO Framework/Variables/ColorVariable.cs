﻿using UnityEngine;
using UnityEngine.Networking;

[CreateAssetMenu(menuName = "SO Variables/Color")]
public class ColorVariable : VariableBase
{
    [SerializeField, HideInInspector]
    private Color32 _defaultValue;

    [SerializeField, HideInInspector]
    private Color32 _currentValue;

    public Color32 Value => _currentValue;

    protected void OnEnable()
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
        _currentValue = _defaultValue;
    }

    public void SetValue(Color32 newValue)
    {
        _currentValue = newValue;
        OnValueChanged.SafeInvoke();
    }

    public void SetValue(ColorVariable newValue)
    {
        _currentValue = newValue.Value;
        OnValueChanged.SafeInvoke();
    }

    public override void ResetData()
    {
        _currentValue = _defaultValue;
    }

    protected override void WriteValueData(NetworkWriter writer)
    {
        writer.Write(Value);
    }

    protected override void ReadValueData(NetworkReader reader)
    {
        SetValue(reader.ReadColor32());
    }
}
