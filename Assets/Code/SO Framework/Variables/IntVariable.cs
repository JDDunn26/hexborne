﻿using UnityEngine;
using UnityEngine.Networking;

[CreateAssetMenu(menuName = "SO Variables/Int")]
public class IntVariable : VariableBase
{
    [SerializeField, HideInInspector]
    private int _defaultValue;

    [SerializeField, HideInInspector]
    private int _currentValue;

    public int Value
    {
        get { return _currentValue; }
        set { _currentValue = value;
            _objectValue = value;
        }
    }

    protected void OnEnable()
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
        _currentValue = _defaultValue;
        _objectValue = _defaultValue;
    }

    public void SetValue(int newvalue)
    {
        Value = newvalue;
        OnValueChanged.SafeInvoke();
    }

    public void SetValue(IntVariable newValue)
    {
        Value = newValue.Value;
        OnValueChanged.SafeInvoke();
    }

    public void ApplyChange(int amount)
    {
        Value += amount;
        OnValueChanged.SafeInvoke();
    }

    public void ApplyChange(IntVariable amount)
    {
        Value += amount.Value;
        OnValueChanged.SafeInvoke();
    }

    public override void ResetData()
    {
        _currentValue = _defaultValue;
        _objectValue = _defaultValue;
        OnValueChanged.SafeInvoke();
    }

    protected override void WriteValueData(NetworkWriter writer)
    {
        writer.Write(Value);
    }

    protected override void ReadValueData(NetworkReader reader)
    {
        SetValue(reader.ReadInt32());
    }
}
