﻿using UnityEngine;
using UnityEngine.Networking;

[CreateAssetMenu(menuName = "SO Variables/String")]
public class StringVariable : VariableBase
{
    [SerializeField, HideInInspector]
    private string _defaultValue;

    [SerializeField, HideInInspector]
    private string _currentValue;

    public string Value
    {
        get => _currentValue;
        private set
        {
            _currentValue = value;
            _objectValue = value;
        }
    }

    protected void OnEnable()
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
        _currentValue = _defaultValue;
        _objectValue = _defaultValue;
    }

    public void SetValue(string newValue)
    {
        Value = newValue;
        OnValueChanged.SafeInvoke();
    }

    public void SetValue(StringVariable newValue)
    {
        Value = newValue.Value;
        OnValueChanged.SafeInvoke();
    }

    public void ApplyChange(string amount)
    {
        Value += amount;
        OnValueChanged.SafeInvoke();
    }

    public void ApplyChange(StringVariable amount)
    {
        Value += amount.Value;
        OnValueChanged.SafeInvoke();
    }

    public override void ResetData()
    {
        _currentValue = _defaultValue;
        _objectValue = _defaultValue;
    }

    protected override void WriteValueData(NetworkWriter writer)
    {
        writer.Write(Value);
    }

    protected override void ReadValueData(NetworkReader reader)
    {
        SetValue(reader.ReadString());
    }
}