﻿using UnityEngine;
using System;
using UnityEngine.Networking;

public class VariableBase : ScriptableObject
{
    public Action OnValueChanged;
    public object _objectValue;

#if UNITY_EDITOR
    [TextArea(3, 8)]
    public string devDescription = string.Empty;

    protected void OnValidate()
    {
        OnValueChanged.SafeInvoke();
    }
#endif

    public virtual void ResetData()
    { }

    public void Unload()
    {
        DestroyImmediate(this);
    }

    public void OnSerialize(NetworkWriter writer)
    {
        WriteValueData(writer);
    }

    public void OnDeserialize(NetworkReader reader)
    {
        ReadValueData(reader);
    }

    protected virtual void WriteValueData(NetworkWriter writer)
    { }

    protected virtual void ReadValueData(NetworkReader reader)
    { }

    public object GetObject()
    {
        return _objectValue;
    }
}
