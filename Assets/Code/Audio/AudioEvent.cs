﻿using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Audio Events")]
public class AudioEvent : BaseAudioEvent 
{
	public AudioClip[] clips;

	[MinMaxRange(0, 1)]
	public RangedFloat volume = new RangedFloat(0.5f, 0.5f);

	[MinMaxRange(0, 2)]
	public RangedFloat pitch = new RangedFloat(1.0f, 1.0f);

	public override void Play(AudioSource source)
	{
		if(clips.Length == 0)
			return;

		source.clip = clips[Random.Range(0, clips.Length)];
		source.volume = Random.Range(volume.min, volume.max);
		source.pitch = Random.Range(pitch.min, pitch.max);
		source.Play();
	}
}
