﻿using UnityEngine;

public abstract class BaseAudioEvent : ScriptableObject 
{
	public abstract void Play(AudioSource source);
}
