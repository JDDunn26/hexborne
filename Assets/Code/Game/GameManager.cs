﻿using System.Collections;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public HexGrid Grid;
	public IntVariable NumberOfMoves;
	public IntVariable BestScore;

	public BoolVariable HasWon;

	public AudioEvent WinSound;
	public AudioSource AmbianceSource;
	public AudioLowPassFilter MusicLowPassFilter;

	public TextMeshProUGUI WinText;
	private bool _trackingBest;
	public GameEvent HideWinDisplay;
	public GameEvent StartTrackingBestEvent;
	public GameEvent StopTrackingBestEvent;

	[UsedImplicitly] // By Animation Event
	public void PerformIntroSequence()
	{
		Grid.PerformIntroSequence();
	}

	[UsedImplicitly] // By UI Button
	public void ShuffleGrid()
	{
		if (HasWon.Value)
		{
			HideWinDisplay.Invoke();
			HasWon.Value = false;
		}
		
		if (_trackingBest)
		{
			StopTrackingBestEvent.Invoke();
			_trackingBest = false;
		}

		_trackingBest = false;
		
		Grid.ShuffleHexes();

		for (int i = 0; i < Grid.Hexes.Count; i++)
		{
			Grid.Hexes[i].LerpToHexSlot(Grid.Hexes[i].originalPosition, 0.5f);
		}

		StorageUtil.SaveGame(Grid.Hexes);
		NumberOfMoves.SetValue(0);
		BestScore.SetValue(0);
	}

	[UsedImplicitly] // By UI Button
	public void Restart()
	{
		if (HasWon.Value)
		{
			HideWinDisplay.Invoke();
			HasWon.Value = false;
		}
		
		StorageUtil.Save save = StorageUtil.LoadGame();

		for (int i = 0; i < Grid.Hexes.Count; i++)
		{
			Grid.Hexes[i].LerpToHexSlot(Grid.HexSlots[save.hexData[i].SavedSlot].transform.position, 0.5f);

			if (save.hexData[i].TargetHex != Grid.Hexes[i].TargetSlot)
			{
				Debug.Log("Hexes don't match - something is wrong!");
			}

			Grid.Hexes[i].Slot = Grid.HexSlots[save.hexData[i].SavedSlot];
		}
		
		NumberOfMoves.SetValue(0);
	}

	[UsedImplicitly] // By GameEvent Listener trigger function
	public void OnWinEvent()
	{
		if (!_trackingBest)
		{
			BestScore.SetValue(NumberOfMoves.Value);

			StartTrackingBestEvent.Invoke();
			_trackingBest = true;
			WinText.text = "You Win!";
		}
		else
		{
			if (NumberOfMoves.Value < BestScore.Value)
			{
				BestScore.SetValue(NumberOfMoves.Value);
				WinText.text = "New Best!";
			}
			else
			{
				WinText.text = "Oops!";
			}
		}

		HasWon.Value = true;

		StartCoroutine(LowCutMusic(3, 4));
		WinSound.Play(AmbianceSource);
	}

	[UsedImplicitly] // By Animation Event
	public void PlayAmbiance()
	{
		StartCoroutine(LowCutMusic(3, 4));
		WinSound.Play(AmbianceSource);
	}

	public IEnumerator LowCutMusic(float cutSpeed, float cutTime)
	{
		const int floor = 500;
		const int ceiling = 22000;
		
		float elapsedTime = 0;

		while (elapsedTime < cutSpeed)
		{
			elapsedTime += Time.deltaTime;
			MusicLowPassFilter.cutoffFrequency = Mathf.SmoothStep(MusicLowPassFilter.cutoffFrequency, floor, elapsedTime / cutSpeed);
			yield return new WaitForEndOfFrame();
		}

		elapsedTime = 0;
		
		while (elapsedTime < cutTime)
		{
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		elapsedTime = 0;
		
		while (elapsedTime < cutSpeed)
		{
			elapsedTime += Time.deltaTime;
			MusicLowPassFilter.cutoffFrequency = Mathf.SmoothStep(MusicLowPassFilter.cutoffFrequency, ceiling, elapsedTime / cutSpeed);
			yield return new WaitForEndOfFrame();
		}
	}

	[UsedImplicitly]
	public void QuitGame()
	{
		Application.Quit();
	}
}
