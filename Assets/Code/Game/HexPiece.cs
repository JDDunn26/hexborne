﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HexPiece : MonoBehaviour
{
	public Image Image;
	public HexSlot Slot;
	public int TargetSlot;

	public Vector3 originalPosition;
	public Vector3 vectorFromCentre;
	public Vector3 offScreenPosition;
	
	public IEnumerator lerpToNewHexCoroutine;
	public IEnumerator lerpToColorCoroutine;
	
	void Awake()
	{
		// This ensures consistent hover state.
		Image = GetComponent<Image>();
		Image.alphaHitTestMinimumThreshold = 0.2f;
	}

	public void Setup(int targetSlot, HexSlot slot, Sprite sprite)
	{
		TargetSlot = targetSlot;
		Slot = slot;
		Image.sprite = sprite;
	}

	public void LerpToHexSlot(Vector3 newTransform, float lerpTime)
	{
		if (lerpToNewHexCoroutine != null)
		{
			StopCoroutine(lerpToNewHexCoroutine);
		}

		lerpToNewHexCoroutine = LerpToNewHexCoroutine(newTransform, lerpTime);
		StartCoroutine(lerpToNewHexCoroutine);
	}

	private IEnumerator LerpToNewHexCoroutine(Vector3 position, float lerpTime)
	{        
		float elapsedTime = 0;

		while (elapsedTime < lerpTime)
		{
			elapsedTime += Time.deltaTime;
			transform.position = Vector3.Lerp(transform.position, position, elapsedTime / lerpTime);
			yield return new WaitForEndOfFrame();
		}
	}
	
	public void LerpToColor(Color color, float lerpTime)
	{
		if (lerpToColorCoroutine != null)
		{
			StopCoroutine(lerpToColorCoroutine);
		}

		lerpToColorCoroutine = LerpToColorCoroutine(color, lerpTime);
		StartCoroutine(lerpToColorCoroutine);
	}
	
	public void LerpToColorFrom(Color from, Color to, float lerpTime)
	{
		Image.color = from;
		
		if (lerpToColorCoroutine != null)
		{
			StopCoroutine(lerpToColorCoroutine);
		}

		lerpToColorCoroutine = LerpToColorCoroutine(to, lerpTime);
		StartCoroutine(lerpToColorCoroutine);
	}
	
	private IEnumerator LerpToColorCoroutine(Color color, float lerpTime)
	{        
		float elapsedTime = 0;

		while (elapsedTime < lerpTime)
		{
			elapsedTime += Time.deltaTime;
			Image.color = Color.Lerp(Image.color, color, elapsedTime / lerpTime);
			yield return new WaitForEndOfFrame();
		}
	}
}
