﻿using JetBrains.Annotations;
using TMPro;
using UnityEngine;

public class AnimatedCounter : MonoBehaviour
{
    public IntVariable Counter;
    public TextMeshProUGUI CounterDisplay;
    public Animator Animator;

    public bool BindValueToText = true;
    
    public void Start()
    {
        if (BindValueToText)
        {
            Counter.OnValueChanged += UpdateDisplayText;
        }
    }

    private void OnDestroy()
    {
        if (BindValueToText)
        {
            Counter.OnValueChanged -= UpdateDisplayText;
        }
    }
    
    [UsedImplicitly] // By SO Event Listener Trigger
    public void IncrementMovesCounter()
    {
        Counter.ApplyChange(1);
    }

    public void UpdateDisplayText()
    {
        CounterDisplay.text = Counter.Value.ToString();
        Animator.Play("IncrementCounter");
    }
}