﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HexGrid : MonoBehaviour
{
	private readonly Vector2[] _hexDirections =
	{
		new Vector2(1, 0),
		new Vector2(1, -1),
		new Vector2(0, -1),
		new Vector2(-1, 0),
		new Vector2(-1, 1),
		new Vector2(0, 1)
	};

	public Transform HexesParent;

	public HexPiece hexPiece;
	
	public List<HexSlot> HexSlots;
	public Sprite[] HexSprites;
	
	[HideInInspector] public List<HexPiece> Hexes = new List<HexPiece>();
	private readonly List<HexSlot> _dirtyList = new List<HexSlot>();

	public Transform centralHex;

	public List<int> spiralOrder;

	public Animator winAnimator;
	public ParticleSystem winParticles;
	public GameEvent winEvent;
	
	public AudioEvent swooshSound;
	public AudioSource audioSource;

	private void Start()
	{
		SetupFirstGrid();		
		CalculateHexTrajectory();
	}

	public void PerformIntroSequence()
	{		
		StartCoroutine(SpiralHexesOut());
	}

	private void CalculateHexTrajectory()
	{
		foreach (var hex in Hexes)
		{
			hex.originalPosition = hex.transform.position;
			hex.vectorFromCentre = hex.transform.position - centralHex.transform.position;
			hex.offScreenPosition = hex.originalPosition + hex.vectorFromCentre * 10;
		}
	}
	
	private IEnumerator SpiralHexesOut()
	{			
		for (int i = 0; i < Hexes.Count; i++)
		{
			if (spiralOrder[i] == 10)
			{
				Hexes[spiralOrder[i] - 1].LerpToColorFrom(Color.white, Color.clear, 1);
			}
			else
			{
				Hexes[spiralOrder[i] - 1].LerpToHexSlot(Hexes[spiralOrder[i] - 1].offScreenPosition, 5);				
			}

			swooshSound.Play(audioSource);
			
			yield return new WaitForSeconds(0.1f);
		}
		
		ShuffleHexes();
		StartCoroutine(SpiralHexesIn());
	}

	private IEnumerator SpiralHexesIn()
	{
		yield return new WaitForSeconds(2f);
		
		spiralOrder.Reverse();
		
		for (int i = 0; i < Hexes.Count; i++)
		{
			if (spiralOrder[i] == 10)
			{
				Hexes[spiralOrder[i] - 1].Image.color = Color.white;
			}
			
			Hexes[spiralOrder[i] - 1].LerpToHexSlot(Hexes[spiralOrder[i] - 1].originalPosition, 2);
			
			swooshSound.Play(audioSource);

			yield return new WaitForSeconds(0.1f);
		}
		
		yield return new WaitForSeconds(2);

		for (int i = 0; i < HexSlots.Count; i++)
		{
			HexSlots[i].drawer.TintOutlineColor(Color.clear, 1.5f);
		}

		StorageUtil.SaveGame(Hexes);
	}

	public void OutlineSelected(HexPiece hex)
	{
		hex.Slot.drawer.SelectedOutline();
		_dirtyList.Add(hex.Slot);
	}

	public void OutlineNeighbours(HexPiece piece)
	{
		foreach (var hex in HexSlots)
		{
			foreach (var direction in _hexDirections)
			{
				if (hex.coordinate != piece.Slot.coordinate + direction) 
					continue;

				if (hex.index == piece.TargetSlot)
				{
					hex.drawer.HintOutline();
				}
				else
				{
					hex.drawer.NeighbourOutline();
				}
				
				_dirtyList.Add(hex);
			}
		}	
	}

	public void ClearOutlineList()
	{
		foreach (var hex in _dirtyList)
		{
			hex.drawer.NormalizeOutline();
		}
		
		_dirtyList.Clear();
	}

	public void SetupFirstGrid()
	{
		for(int i = 0; i < HexSlots.Count; i++)
		{	
			var hex = Instantiate(hexPiece, HexesParent, false);

			hex.transform.position = HexSlots[i].transform.position;
			HexSlots[i].index = i;
			hex.Setup(i, HexSlots[i], HexSprites[i]);
			Hexes.Add(hex);
		}
		
		StorageUtil.SaveGame(Hexes);
	}
	
	public void ShuffleHexes()
	{
		var scrambled = Enumerable
			.Range(0, HexSlots.Count)
			.OrderBy(_ => Random.value)
			.ToArray();

		for(int targetSlot = 0; targetSlot < HexSlots.Count; targetSlot++)
		{
			var slot = scrambled[targetSlot];

			Hexes[targetSlot].originalPosition = HexSlots[slot].transform.position;
			HexSlots[slot].index = slot;
			Hexes[targetSlot].Setup(targetSlot, HexSlots[slot], HexSprites[targetSlot]);
		}
	}

	public void CheckForWinState()
	{
		foreach (var hex in Hexes)
		{
			if (hex.TargetSlot != hex.Slot.index)
			{
				return;
			}
		}

		Debug.Log("You Win!");
		winAnimator.Play("Win");
		winParticles.Play();
		winEvent.Invoke();
		ClearOutlineList();
	}
}
