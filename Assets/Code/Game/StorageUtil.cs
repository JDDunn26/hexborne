﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class StorageUtil
{
    [Serializable]
    public struct HexData
    {
        public int SavedSlot;
        public int TargetHex;

        public HexData(int savedSlot, int targetHex)
        {
            SavedSlot = savedSlot;
            TargetHex = targetHex;
        }
    }

    private static string json;

    public struct Save
    {
        public List<HexData> hexData;
    }

    public static void SaveGame(List<HexPiece> pieces)
    {
        Save save = new Save {hexData = new List<HexData>()};

        foreach (var piece in pieces)
        {
            save.hexData.Add(new HexData(piece.Slot.index, piece.TargetSlot));
        }
        
        json = JsonUtility.ToJson(save);
    }

    public static Save LoadGame()
    {      
        return JsonUtility.FromJson<Save>(json);
    }
}
