﻿using UnityEngine;

[CreateAssetMenu(menuName = "Hex/Colors")]
public class HexColors : ScriptableObject
{
    public Color normal;
    public Color neighbour;
    public Color selected;
    public Color hint;
}
