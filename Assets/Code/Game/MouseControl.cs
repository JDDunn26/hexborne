﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class MouseControl : MonoBehaviour
{
    public GraphicRaycaster Raycaster;
    private PointerEventData _pointerEventData;
    public EventSystem EventSystem;

    public BoolVariable HasWon;
    
    private HexPiece _hexHovered;
    private HexPiece _hexSelected;
    private HexPiece _hexToReplace;

    public HexGrid Grid;
    private Vector3 _offset;

    private readonly List<RaycastResult> _results = new List<RaycastResult>();
    
    public GameEvent MadeMoveEvent;
    
    public AudioEvent HoverSound;
    public AudioEvent SelectSound;
    public AudioEvent SwitchSound;
    public AudioEvent CorrectHex;

    public ParticleSystem CorrectHexParticle;
    
    public AudioSource EffectsAudioSource;
    public AudioSource AmbianceAudioSource;

    void Update()
    {
        if (!HasWon.Value)
        {
            _pointerEventData = new PointerEventData(EventSystem)
            {
                position = Input.mousePosition
            };

            _results.Clear();

            Raycaster.Raycast(_pointerEventData, _results);

            if (Input.GetKeyDown(KeyCode.Mouse0)) // Left Mouse Button Down Event
            {
                foreach (RaycastResult result in _results)
                {
                    HexPiece hexPiece = result.gameObject.GetComponent<HexPiece>();

                    if (hexPiece != null)
                    {
                        _hexSelected = hexPiece;
                        _offset = _hexSelected.transform.position - Input.mousePosition;
                        Grid.OutlineSelected(_hexSelected);
                        Grid.OutlineNeighbours(_hexSelected);
                        SelectSound.Play(EffectsAudioSource);
                        break;
                    }
                }
            }

            if (Input.GetKey(KeyCode.Mouse0))
            {
                if (_hexSelected != null)
                {
                    _hexSelected.transform.position = Input.mousePosition + _offset;

                    foreach (RaycastResult result in _results)
                    {
                        HexPiece hexPiece = result.gameObject.GetComponent<HexPiece>();

                        if (hexPiece != null && hexPiece != _hexSelected &&
                            hexPiece.Slot.drawer.state != HexDrawState.Normal)
                        {
                            _hexToReplace = hexPiece;
                            HoverSound.Play(EffectsAudioSource);

                            SwitchOut();

                            break; // Candidate hex to switch has been found so stop iterating.
                        }
                    }
                }
            }
            else
            {
                foreach (RaycastResult result in _results)
                {
                    HexPiece hexPiece = result.gameObject.GetComponent<HexPiece>();

                    if (hexPiece != null && hexPiece != _hexHovered)
                    {
                        Grid.ClearOutlineList();

                        _hexHovered = hexPiece;
                        Grid.OutlineSelected(_hexHovered);
                        HoverSound.Play(EffectsAudioSource);
                        break;
                    }
                }
            }

            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                if (_hexSelected != null)
                {
                    _hexSelected.LerpToHexSlot(_hexSelected.Slot.transform.position, 0.5f);
                    _hexSelected = null;
                }

                if (_hexToReplace != null)
                {
                    _hexToReplace.LerpToHexSlot(_hexToReplace.Slot.transform.position, 0.5f);
                    _hexToReplace = null;
                }

                Grid.ClearOutlineList();
            }

            if (_results.Count == 0)
            {
                _hexHovered = null;
            }
        }
    }
    
    public void SwitchOut()
    {
        _hexSelected.LerpToHexSlot(_hexToReplace.Slot.transform.position, 0.5f);
        _hexToReplace.LerpToHexSlot(_hexSelected.Slot.transform.position, 0.5f);

        if (_hexSelected.TargetSlot == _hexToReplace.Slot.index)
        {
            CorrectHexParticle.transform.position = _hexToReplace.Slot.transform.position;
            CorrectHexParticle.Stop();
            CorrectHexParticle.Play();
            CorrectHex.Play(AmbianceAudioSource);
        }
        
        HexSlot newSlot = _hexToReplace.Slot;
        HexSlot oldSlot = _hexSelected.Slot;

        _hexSelected.Slot = newSlot;
        _hexToReplace.Slot = oldSlot;
        
        MadeMoveEvent.Invoke();
        
        SwitchSound.Play(EffectsAudioSource);
        
        Grid.ClearOutlineList();
     
        Grid.OutlineSelected(_hexSelected);
        
        _hexSelected = null;
        _hexToReplace = null;
        
        Grid.CheckForWinState();
    }
}