﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public enum HexDrawState
{
    Normal,
    Neighbouring,
    Selected,
    Hint
}

public class HexDrawer : MonoBehaviour
{
    public HexDrawState state = HexDrawState.Normal;
    public Image hexOutline;

    public HexColors outlineColors;

    public IEnumerator outlineColorChangeCoroutine;

    public void TintOutlineColor(Color tintColor, float lerpLength)
    {
        if (outlineColorChangeCoroutine != null)
        {
            StopCoroutine(outlineColorChangeCoroutine);
        }

        outlineColorChangeCoroutine = TintOutlineCoroutine(tintColor, lerpLength);
        StartCoroutine(outlineColorChangeCoroutine);
    }

    private IEnumerator TintOutlineCoroutine(Color tintColor, float lerpLength)
    {
        float lerpTime = lerpLength;
        
        float elapsedTime = 0;

        while (elapsedTime < lerpTime)
        {
            elapsedTime += Time.deltaTime;
            hexOutline.color = Color.Lerp(hexOutline.color, tintColor, elapsedTime / lerpTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public void NormalizeOutline()
    {
        if (state == HexDrawState.Normal) 
            return;
        
        state = HexDrawState.Normal;
        TintOutlineColor(outlineColors.normal, 1.5f);
    }
    
    public void NeighbourOutline()
    {
        if (state == HexDrawState.Neighbouring) 
            return;
        
        state = HexDrawState.Neighbouring;
        TintOutlineColor(outlineColors.neighbour, 0.5f);
    }

    public void SelectedOutline()
    {
        if (state == HexDrawState.Selected) 
            return;
        
        state = HexDrawState.Selected;
        TintOutlineColor(outlineColors.selected, 0.5f);
    }
    
    public void HintOutline()
    {
        if (state == HexDrawState.Hint) 
            return;
        
        state = HexDrawState.Hint;
        TintOutlineColor(outlineColors.hint, 0.5f);
    }
}
