﻿using UnityEngine;

public class HexSlot : MonoBehaviour
{
    public int index;
    public RectTransform hexTransform;
    public Vector2 coordinate; // Note: I went from Axial coords over Cube as they matched Starborne's Vector2 coord approach.
    public HexDrawer drawer; // Note: I went from a drawer class as this helps separate out the business logic from the visuals.
}
