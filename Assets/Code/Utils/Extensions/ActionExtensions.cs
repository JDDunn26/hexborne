﻿using System;

#if NET_4_6
using System.Runtime.CompilerServices;
#endif

public static class ActionExtensions
{
#if NET_4_6
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public static void SafeInvoke(this Action action)
        {
#if NET_4_6
            action?.Invoke();
#else
            var actionHandler = action;
            if (actionHandler != null)
                actionHandler();
#endif
        }

#if NET_4_6
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public static void SafeInvoke<T1>(this Action<T1> action, T1 var1)
        {
#if NET_4_6
            action?.Invoke(var1);
#else
            var actionHandler = action;
            if (actionHandler != null)
                actionHandler(var1);
#endif
        }

#if NET_4_6
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 var1, T2 var2)
        {
#if NET_4_6
            action?.Invoke(var1, var2);
#else
            var actionHandler = action;
            if (actionHandler != null)
                actionHandler(var1, var2);
#endif
        }

#if NET_4_6
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 var1, T2 var2, T3 var3)
        {
#if NET_4_6
            action?.Invoke(var1, var2, var3);
#else
            var actionHandler = action;
            if (actionHandler != null)
                actionHandler(var1, var2, var3);
#endif
        }

#if NET_4_6
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
        public static void SafeInvoke<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> action, T1 var1, T2 var2, T3 var3, T4 var4)
        {
#if NET_4_6
            action?.Invoke(var1, var2, var3, var4);
#else
            var actionHandler = action;
            if (actionHandler != null)
                actionHandler(var1, var2, var3, var4);
#endif
        }
}

