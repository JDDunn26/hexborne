﻿using UnityEngine;

public class ColorCycler : MonoBehaviour
{
	public Color startColor = Color.red;
	
	public Material matToColor;
		
	public float time = 10.0f;
	private HSBColor hsbc;

	[HideInInspector] public Color cycledColor;
		 
	void Start() 
	{
		hsbc = HSBColor.FromColor(startColor);
	}

	void Update()
	{
		hsbc.h = (hsbc.h + Time.deltaTime / time) % 1.0f;
		
		cycledColor = HSBColor.ToColor(hsbc);

		if (matToColor != null)
		{
			matToColor.color = cycledColor;
		}
	}
}
